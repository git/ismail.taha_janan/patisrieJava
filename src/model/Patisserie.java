package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Patisserie {
    public List<Gateau> gateauList = new ArrayList();


    synchronized void depose(){
        gateauList.add(new Gateau());
    }

    synchronized void mange(){
        gateauList.remove(0);
    }

    synchronized int gateauxReste() {
        return gateauList.size();
    }



}
